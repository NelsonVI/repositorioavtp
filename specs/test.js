const assert = require('assert').strict;

import autoCompra from '../pageObjects/autoCompra.Page'
import datosCompra from '../pageObjects/datosCompra.Page'

describe('Avantrip Ejercicio', () => {
	it('Pasos para rentar un auto', () => {
        /*
        En test.js instancio todo y agrego argumentos para que sea facil de modificar.
        */
        autoCompra.abrirBrowser()
        autoCompra.IframeCerrar()
        autoCompra.completarCiudadDeRetiro("Mendoza")
        //Para completar fecha y hora seguir el con formato "dd/MM/yyyy" para la fecha y la hora es 24hs con el formato "12:00" 
        autoCompra.completarFechaHoraDesde("29/07/2019","13:00")
        autoCompra.completarFechaHoraHasta("31/07/2019","11:00")
        autoCompra.clickBuscarAuto()
        autoCompra.elegirOpcionDeCompra()
        //Solo darle tiempo de carga a la WEB
        browser.pause(1000) 
        //Completar con nombre, apellido, dia, mes, anio, dni como parametros
        datosCompra.completarQuienVaAConducir("Juan","Matienzo","12","05","1985","20878555")
        //Completar con nombre, mail, area, telefono
        datosCompra.completarDatosContacto("Juan Matienzo","jmatienzo@mail.com","11","15978412")
        //Completar con banco, cuotas, numeroTC, codSeg, mes, anio. 
        datosCompra.completarDatosTarjeta("Banco Ciudad","12","4502080879895621","123","9","22")
        //Completar con nombreCompleto, dni, FechaNac, domicilio
        datosCompra.completarDatosTitularTarjeta("Juan Matienzo","20878555","12/05/1985","Caseros 4585")
       //Para chequear los datos completos
        browser.pause(2000) 
        /**
         * VERIFICACIONES: Tome el texto del form para ingresar la tarjeta de credito, para que hacer assert y ver que
         * realmente esta en la pagina. Lo mismo con terminos y condiciones.
         */
        var terminosYcondiciones = $('//*[@id="FormTarjeta"]/div[3]/div/ng-include[2]/div/label').getText()
        var datosDeLaTarjeta = $('//*[@id="FormTarjeta"]/div[1]/h4')

        assert.strictEqual(datosDeLaTarjeta,'Datos de la tarjeta')
        assert.strictEqual(terminosYcondiciones,'Declaro conocer y aceptar los Términos y condiciones de compra') 
    })
})
/// Para correr los test en terminal-----> ./node_modules/.bin/wdio wdio.conf.js
